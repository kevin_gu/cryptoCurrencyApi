package com.crypto.repository;

import com.crypto.TestAccountApplication;
import com.crypto.domain.dto.CoinDescriptionEntity;
import com.crypto.domain.dto.CoinEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestAccountApplication.class)
@Slf4j
@Transactional
public class CoinDescriptionRepositoryIT {

    @Autowired
    private CoinDesriptionRepository coinDesriptionRepository;

    @Test
    public void findCoinById() {
        List<CoinDescriptionEntity> coinDescriptionEntities = coinDesriptionRepository.findByName("fake coin");
        assertThat(coinDescriptionEntities).isNotEmpty();
    }

    @Test
    public void saveCoinDescription() {


        CoinDescriptionEntity expect = new CoinDescriptionEntity();


        expect.setName("bit coin");

        expect.setShortDescription("s");
        expect.setLongDescription("l");
        expect.setCreateTime(Timestamp.valueOf(LocalDateTime.now()));

        CoinDescriptionEntity result = coinDesriptionRepository.save(expect);
        assertThat(result).isNotNull();
        assertThat(result.getId()).isGreaterThan(0);
    }


    @Test
    public void updateCoin() {
        coinDesriptionRepository.updateDescription("fake coin",
                "short description",
                "long description long description......",
                Timestamp.valueOf(LocalDateTime.now()));
    }
}
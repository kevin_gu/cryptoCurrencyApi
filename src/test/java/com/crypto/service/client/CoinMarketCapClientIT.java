package com.crypto.service.client;

import com.crypto.TestAccountApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestAccountApplication.class)
public class CoinMarketCapClientIT {
    @Autowired
    private CoinMarketCapClient coinMarketCapClient;

    @Test
    public void getCoinList() throws Exception {
        assertThat(coinMarketCapClient.getCoinList().size()).isEqualTo(1385);
    }

}
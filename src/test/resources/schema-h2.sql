CREATE TABLE ACCOUNT(
  id   BIGINT IDENTITY PRIMARY KEY,
  name VARCHAR (20)     NOT NULL,
  wechatId  CHAR (20)              NOT NULL,
  address  VARCHAR (25) ,
  createTime   TIMESTAMP NOT NULL,
  lastActivityTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (ID)
);

ALTER TABLE ACCOUNT ADD CONSTRAINT NAME_UNIQUE UNIQUE(name);
ALTER TABLE ACCOUNT ADD CONSTRAINT WECHAT_UNIQUE UNIQUE(wechatId);



DROP TABLE IF EXISTS coin;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE coin (
  id   BIGINT IDENTITY PRIMARY KEY,
  symbol VARCHAR(50) NOT NULL,
  name VARCHAR(128) NOT NULL,
  display_name VARCHAR(128) NOT NULL,
  logo VARCHAR(100) NOT NULL,
  market_cap DECIMAL(20, 2) NOT NULL,
  price DECIMAL(20, 2) NOT NULL,
  volume24h DECIMAL(20, 2) NOT NULL,
  supply DECIMAL(20, 2) NOT NULL,
  percentage7d DECIMAL(20, 2) NOT NULL,
  percentage24h DECIMAL(20, 2) NOT NULL,

  create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  account_enabled BIT(1) DEFAULT 1 NOT NULL,
  PRIMARY KEY (ID)
) ;

ALTER TABLE coin ADD CONSTRAINT un_name1 UNIQUE(name);


DROP TABLE IF EXISTS coin_description;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE coin_description (
  id BIGINT IDENTITY PRIMARY KEY,
  name VARCHAR(128) NOT NULL,
  long_description VARCHAR(12800) DEFAULT NULL,
  short_description VARCHAR(480) DEFAULT NULL,
  create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (ID)
) ;

ALTER TABLE coin_description ADD CONSTRAINT un_name2 UNIQUE(name);

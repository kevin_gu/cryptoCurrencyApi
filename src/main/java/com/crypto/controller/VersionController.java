package com.crypto.controller;

import java.util.ResourceBundle;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class VersionController {

    private final static String RESOURCE_BUNDLE_NAME = "mvnbuild";
    private final static String VERSION =
            ResourceBundle.getBundle(RESOURCE_BUNDLE_NAME).getString("project.version") + " [" + ResourceBundle
                    .getBundle(RESOURCE_BUNDLE_NAME).getString("build.timestamp") + "]";

    @RequestMapping(value = "/version", method = RequestMethod.GET)
    @ResponseBody
    public String getVersion() {
        return VERSION;
    }
}

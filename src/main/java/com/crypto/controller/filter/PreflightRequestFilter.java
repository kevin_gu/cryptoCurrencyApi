package com.crypto.controller.filter;

import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN;

public class PreflightRequestFilter extends GenericFilterBean {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		if(httpRequest.getMethod().equalsIgnoreCase("OPTIONS")){
            ((HttpServletResponse)response).addHeader(ACCESS_CONTROL_ALLOW_ORIGIN, "*");
            ((HttpServletResponse)response).setStatus(200);
            ((HttpServletResponse)response).setHeader("Access-Control-Allow-Credentials", "true");
            ((HttpServletResponse)response).setHeader("Access-Control-Allow-Origin", "*");
            ((HttpServletResponse)response).setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
            ((HttpServletResponse)response).setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type,x-auth-token, Accept,X-Requested-With");
            ((HttpServletResponse)response).setHeader("Access-Control-Max-Age", "1728000");
            ((HttpServletResponse)response).setHeader("Content-Type", "text/plain");
		}else{
            ((HttpServletResponse)response).addHeader(ACCESS_CONTROL_ALLOW_ORIGIN, "*");
            chain.doFilter(request, response);
        }

	}
}
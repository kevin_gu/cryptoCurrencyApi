package com.crypto.controller;

import com.crypto.domain.Coin;
import com.crypto.domain.CoinDescription;
import com.crypto.service.CoinService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class CoinController {

    private final CoinService coinService;

    @RequestMapping(value = "/coin/pull", method = RequestMethod.POST)
    @ResponseBody
    public boolean getVersion() {
        return coinService.pullFromCoinMarketCap();
    }

    @RequestMapping(value = "/coin/all", method = RequestMethod.GET)
    @ResponseBody
    public List<Coin> getAllCoin() {
        return coinService.getAllCoin();
    }

    @RequestMapping(value = "/coins", method = RequestMethod.GET)
    @ResponseBody
    public List<Coin> getCoinsByEnabled(@RequestParam(value = "enabled") boolean enabled) {
        return coinService.getCoinsByEnabled(enabled);
    }

    @RequestMapping(value = "/coins/search", method = RequestMethod.GET)
    @ResponseBody
    public List<Coin> getCoinsByEnabledAndKeyword(@RequestParam(value = "enabled") boolean enabled,@RequestParam(value = "keyword") String keyword) {
        return coinService.getCoinsByEnabledAndKeyword(enabled,keyword);
    }


    @RequestMapping(value = "/coin", method = RequestMethod.GET)
    @ResponseBody
    public Coin getCoinByName(@RequestParam(value = "name") String name) {
        return coinService.getCoinByName(name);
    }

    @RequestMapping(value = "/coin/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Coin getCoinById(@PathVariable int id){
        return coinService.getCoinById(id);
    }


    @RequestMapping(value = "/coin/enable", method = RequestMethod.POST)
    @ResponseBody
    public boolean enableCoin(@RequestBody CoinDescription coinDescription) {
        return coinService.updateEnabled(coinDescription,true);
    }

    @RequestMapping(value = "/coin/disable", method = RequestMethod.POST)
    @ResponseBody
    public boolean disableCoin(@RequestBody CoinDescription coinDescription) {
        return coinService.updateEnabled(coinDescription,false);
    }

///*description apis*/
//    @RequestMapping(value = "/coin/updateDescription", method = RequestMethod.POST)
//    @ResponseBody
//    public boolean updateDescription(@RequestParam(value = "name") String name,
//                                     @RequestParam(value = "shortDescription") String shortDescription,
//                                     @RequestParam(value = "longDescription") String longDescription) {
//        return coinService.updateDescriptionForCoin(name,shortDescription,longDescription);
//    }

    @RequestMapping(value = "/coin/description", method = RequestMethod.GET)
    @ResponseBody
    public CoinDescription getCoinDescriptionByName(@RequestParam(value = "name") String name) {
        return coinService.getDescriptionForCoin(name);
    }

    @RequestMapping(value = "/coin/description/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CoinDescription getCoinDescriptionById(@PathVariable int id) {
        return coinService.getDescriptionForCoinById(id);
    }
}

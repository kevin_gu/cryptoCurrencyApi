package com.crypto.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class BatchRunnableStatus {
    private LocalDateTime lastRun;
    private boolean on;
    private boolean working;

}

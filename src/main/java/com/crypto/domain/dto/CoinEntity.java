package com.crypto.domain.dto;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;


@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "COIN", uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
public class CoinEntity implements Serializable {

//https://www.cryptocompare.com/media/20627/nxt.png?width=50
//    Logo, Ticker symbol, Market cap,
//    Price (direct pull from CMC's price),
//            Volume (24h, direct pull from CMC),
//    Circulating supply (direct pull from CMC),
//    Percentage change - in both 24H & 7D format (direct pull from CMC);
//
//    {
//        "id": "bitcoin",
//            "name": "Bitcoin",
//            "symbol": "BTC",
//            "rank": "1",
//            "price_usd": "573.137",
//            "price_btc": "1.0",
//            "24h_volume_usd": "72855700.0",
//            "market_cap_usd": "9080883500.0",
//            "available_supply": "15844176.0",
//            "total_supply": "15844176.0",
//            "percent_change_1h": "0.04",
//            "percent_change_24h": "-0.3",
//            "percent_change_7d": "-0.57",
//            "last_updated": "1472762067"
//    },

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 1, max = 10)
    private String symbol;

    @NotNull
    private String name;

    @NotNull
    private String displayName;

    @NotNull
    private String logo;

    @NotNull
    private BigDecimal marketCap;

    @NotNull
    private BigDecimal price;

    @NotNull
    private BigDecimal volume24h;

    @NotNull
    private BigDecimal supply;

    @NotNull
    private BigDecimal percentage24h;

    @NotNull
    private BigDecimal percentage7d;

    @NotNull
    @Column(name="accountEnabled")
    private boolean enabled;


    @Column(name = "createTime",insertable = true, updatable = true,nullable=false,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp createTime;


}

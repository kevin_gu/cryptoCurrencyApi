package com.crypto.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class CMCCoin {
    private String id;
    private String name;
    private String symbol;

    @JsonProperty(value="price_usd")
    @NotNull
    private BigDecimal price;

    @JsonProperty("market_cap_usd")
    @NotNull
    private BigDecimal marketCap;

    @JsonProperty("available_supply")
    @NotNull
    private BigDecimal supply;

    @JsonProperty("24h_volume_usd")
    @NotNull
    private BigDecimal volume;

    @JsonProperty("percent_change_24h")
    @NotNull
    private BigDecimal percentage24h;

    @JsonProperty("percent_change_7d")
    @NotNull
    private BigDecimal percentage7d;

    public void setPrice(BigDecimal price){
        this.price = (price== null ? BigDecimal.ZERO : price);
    }

    public void setMarketCap(BigDecimal marketCap){
        this.marketCap = (marketCap== null ? BigDecimal.ZERO : marketCap);
    }

    public void setSupply(BigDecimal supply){
        this.supply = (supply== null ? BigDecimal.ZERO : supply);
    }

    public void setVolume(BigDecimal volume){
        this.volume = (volume== null ? BigDecimal.ZERO : volume);
    }

    public void setPercentage24h(BigDecimal percentage24h){
        this.percentage24h = (percentage24h== null ? BigDecimal.ZERO : percentage24h);
    }

    public void setPercentage7d(BigDecimal percentage7d){
        this.percentage7d = (percentage7d== null ? BigDecimal.ZERO : percentage7d);
    }

    public BigDecimal getPrice(){
        return price== null ? BigDecimal.ZERO : price;
    }

    public BigDecimal getMarketCap(){
        return marketCap== null ? BigDecimal.ZERO : marketCap;
    }

    public BigDecimal getSupply(){
        return supply== null ? BigDecimal.ZERO : supply;
    }

    public BigDecimal getVolume(){
        return volume== null ? BigDecimal.ZERO : volume;
    }

    public BigDecimal getPercentage24h(){
        return percentage24h== null ? BigDecimal.ZERO : percentage24h;
    }

    public BigDecimal getPercentage7d(){
        return percentage7d== null ? BigDecimal.ZERO : percentage7d;
    }
}

package com.crypto.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenericEntity {

    protected int id;
}

package com.crypto.domain.dto;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;


@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "COIN_DESCRIPTION", uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
public class CoinDescriptionEntity implements Serializable {

    @Id
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String shortDescription;

    @NotNull
    private String longDescription;

    @Column(name = "createTime",insertable = true, updatable = true,nullable=false,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp createTime;


}

package com.crypto.domain;

import com.crypto.domain.dto.CoinDescriptionEntity;
import lombok.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CoinDescription {

    private Long id;

    private String name;

    private String shortDescription;

    private String longDescription;

    private LocalDateTime createTime;

    public static CoinDescription buildFromEntity(CoinDescriptionEntity entity){
        return entity!=null? CoinDescription.builder().id(entity.getId())
                .id(entity.getId())
                .name(entity.getName())
                .shortDescription(entity.getShortDescription())
                .longDescription(entity.getLongDescription())
                .createTime(entity.getCreateTime().toLocalDateTime())
                .build():null;
    }

    public static CoinDescriptionEntity buildEntity(CoinDescription coinDescription){
        return new CoinDescriptionEntity(coinDescription.getId(),
                coinDescription.getName(),
                coinDescription.getShortDescription(),
                coinDescription.getLongDescription(),
                Timestamp.valueOf(coinDescription.getCreateTime()));
    }


    public static List<CoinDescription> buildFromEntity(List<CoinDescriptionEntity> entities) {

       List<CoinDescription> coins = new ArrayList<>();
        for(CoinDescriptionEntity entity:entities){
            coins.add(buildFromEntity(entity));
        }
        return coins;
    }


}

package com.crypto.domain;

import com.crypto.domain.dto.CoinEntity;
import lombok.*;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Coin implements Comparable {

    private Long id;

    private String symbol;

    private String name;
    private String displayName;

    private String logo;

    private String marketCap;

    private String price;

    private String volume24h;

    private String supply;

    private BigDecimal percentage24h;

    private BigDecimal percentage7d;

    private boolean enabled;

    private LocalDateTime createTime;

    private BigDecimal marketCapNumber;

    public static Coin buildFromEntity(CoinEntity entity){
        return entity!=null? Coin.builder().id(entity.getId())
                .name(entity.getName())
                .displayName(entity.getDisplayName())
                .symbol(entity.getSymbol())
                .logo(entity.getLogo())
                .marketCapNumber(entity.getMarketCap())
                .marketCap(toCurrency(entity.getMarketCap()))
                .price(toCurrency(entity.getPrice()))
                .volume24h(toCurrency(entity.getVolume24h()))
                .supply(toCurrency(entity.getSupply()).substring(1,toCurrency(entity.getSupply()).length()))
                .percentage7d(entity.getPercentage7d())
                .percentage24h(entity.getPercentage24h())
                .enabled(entity.isEnabled())
                .createTime(entity.getCreateTime().toLocalDateTime())
                .build():null;
    }

    public static List<Coin> buildFromEntity(List<CoinEntity> entities) {

       List<Coin> coins = new ArrayList<>();
        for(CoinEntity entity:entities){
            coins.add(buildFromEntity(entity));
        }
        return coins;
    }

    @Override
    public int compareTo(Object c) {
        return ((Coin)c).getMarketCapNumber().compareTo(this.getMarketCapNumber());
    }

    private static String toCurrency(BigDecimal money){

        Locale locale = new Locale("en", "US");
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        return currencyFormatter.format(money);
    }
}

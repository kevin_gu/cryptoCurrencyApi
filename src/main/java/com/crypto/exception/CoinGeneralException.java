package com.crypto.exception;


public class CoinGeneralException extends RuntimeException {


    public CoinGeneralException(RequestStage stage, String message) {
        super("stage at "+stage.getStage()+": "+message);
    }

    public enum RequestStage  {
        DATALAYER("unexpected backend data"),
        INPUT("unexpected input"),
        INTERNAL("internal business logic"),
        OTHER("unexpected scenario");

        private final String stage;

        private RequestStage(String stage) {
            this.stage = stage;
        }

        public String getStage() {
            return stage;
        }

    }


}

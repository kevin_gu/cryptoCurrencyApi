package com.crypto.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class DomainUtil {
    public static Object[] getParams(Object entity) {

        Field[] fields = entity.getClass().getDeclaredFields();
        List<Object> parameters = new ArrayList();
        for (Field field : fields) {
            try {
                if (null != PropertyUtils.getSimpleProperty(entity, field.getName())) {
                    parameters.add(PropertyUtils.getSimpleProperty(entity, field.getName()));
                    log.info("ATTRIBUTE VALUE: " + PropertyUtils.getSimpleProperty(entity, field.getName()));
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        return parameters.toArray();

    }

    public static Map<String, Object> getParameterMap(Object entity, boolean withId) {
        Map<String, Object> parameters = new HashMap<>();
        Field[] fields = entity.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                if ((withId || !field.getName().equals("id")) && null != PropertyUtils.getSimpleProperty(entity, field.getName())) {
                    parameters.put(field.getName(), PropertyUtils.getSimpleProperty(entity, field.getName()));
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        return parameters;
    }
    public static List<String> getParameterKey(Object entity, boolean withId) {
        List<String> parameterKeys = new ArrayList<>();
        Field[] fields = entity.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                if ((withId || !field.getName().equals("id")) && null != PropertyUtils.getSimpleProperty(entity, field.getName())) {
                    parameterKeys.add(field.getName());
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        return parameterKeys;
    }
}

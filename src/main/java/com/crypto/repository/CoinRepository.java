package com.crypto.repository;

import com.crypto.domain.dto.CoinEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Repository
@RepositoryRestResource(collectionResourceRel = "coin", path = "coin")
public interface CoinRepository extends JpaRepository<CoinEntity, Long> {
    CoinEntity findById(@Param("id") long id);

    List<CoinEntity> findByEnabled(@Param("enabled") boolean accountEnabled);

    Long countByName(@Param("name") String name);

    Long countBySymbol(@Param("symbol") String symbol);

    List<CoinEntity> findByName(@Param("name") String name);

    List<CoinEntity> findBySymbol(@Param("symbol") String symbol);

    @Modifying(clearAutomatically = true)
    @Query("update CoinEntity coin set coin.enabled =:status where coin.id =:id")
    void updateEnabled(@Param("id") long id,@Param("status") boolean status);

    @Modifying(clearAutomatically = true)
    @Query("update CoinEntity coin set coin.marketCap =:marketCap, " +
            "coin.price =:price, "+
            "coin.volume24h =:volume24h, "+
            "coin.supply =:supply, "+
            "coin.percentage24h =:percentage24h, "+
            "coin.percentage7d =:percentage7d, "+
            "coin.createTime =:createTime " +
            " where coin.name =:name")
    void updateCoin(@Param("name") String name,
                    @Param("marketCap") BigDecimal marketCap,
                    @Param("price") BigDecimal price,
                    @Param("volume24h") BigDecimal volume24h,
                    @Param("supply") BigDecimal supply,
                    @Param("percentage24h") BigDecimal percentage24h,
                    @Param("percentage7d") BigDecimal percentage7d,
                    @Param("createTime") Timestamp createTime);

    List<CoinEntity> findByEnabledAndSymbolIgnoreCase(@Param("enabled") boolean enabled, @Param("symbol") String symbol);
    List<CoinEntity> findByEnabledAndNameIgnoreCase(@Param("enabled") boolean enabled, @Param("name") String name);

}

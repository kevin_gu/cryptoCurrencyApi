package com.crypto.repository;

import com.crypto.domain.dto.CoinDescriptionEntity;
import com.crypto.domain.dto.CoinEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Repository
@RepositoryRestResource(collectionResourceRel = "coin", path = "coin")
public interface CoinDesriptionRepository extends JpaRepository<CoinDescriptionEntity, Long> {


    List<CoinDescriptionEntity> findByName(@Param("name") String name);

    @Modifying(clearAutomatically = true)
    @Query("update CoinDescriptionEntity coinDescription set coinDescription.shortDescription =:shortDescription, " +
            "coinDescription.longDescription =:longDescription," +
            "coinDescription.createTime =:createTime " +
            " where coinDescription.name =:name")
    void updateDescription(@Param("name") String name,
                           @Param("shortDescription") String shortDescription,
                           @Param("longDescription") String longDescription,
                           @Param("createTime") Timestamp createTime);


}

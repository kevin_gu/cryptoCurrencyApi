package com.crypto.repository;

import com.crypto.domain.dto.AccountEntity;
import com.crypto.exception.CoinGeneralException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AccountRepository extends GenericRepository{

    private final AccountMapper accountMapper = new AccountMapper();

    public AccountRepository(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    public RowMapper rowMapper() {
        return accountMapper;
    }

    @Override
    public String tableName() {
        return "ACCOUNT";
    }

    private class AccountMapper implements RowMapper<AccountEntity> {

        @Override
        public AccountEntity mapRow(ResultSet rs, int rowNum) throws SQLException {

            AccountEntity accountEntity = new AccountEntity(
                    rs.getString("name"),
                    rs.getString("wechatId"),
                    rs.getString("address"),
                    rs.getTimestamp("createTime").toLocalDateTime(),
                    rs.getTimestamp("lastActivityTime").toLocalDateTime());
            accountEntity.setId(rs.getInt("id"));
            return accountEntity;
        }

    }

    public List<AccountEntity> getAllAccount(){
        return this.jdbcTemplate.query( "select * from "+tableName(), accountMapper);
    }

    public AccountEntity getAccountByWechatId(String wechatId){

        List<AccountEntity> accountEntities = this.jdbcTemplate.query( "select * from "+tableName()+" where wechatId =?",new Object[] { wechatId }, accountMapper);
        if(accountEntities.size()==1){
            return accountEntities.get(0);
        }else if(accountEntities.size()==0){
            return null;
        }else{
            throw new CoinGeneralException(CoinGeneralException.RequestStage.DATALAYER,"found multiple account with same wechatID");
        }

    }


}

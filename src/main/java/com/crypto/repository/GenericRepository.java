package com.crypto.repository;

import com.crypto.domain.dto.GenericEntity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.List;

import static com.crypto.util.DomainUtil.getParameterKey;
import static com.crypto.util.DomainUtil.getParameterMap;


@AllArgsConstructor
@Slf4j
public abstract class GenericRepository {
    protected final JdbcTemplate jdbcTemplate;

    public <T extends GenericEntity> T create(T t){
        log.info("executing create "+t.getClass().getName());

        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        List<String> paramKeys = getParameterKey(t,false);
        for (String paramKey : paramKeys)
        {
            log.info("key = "+ paramKey);
        }
        jdbcInsert.withTableName(this.tableName()).usingColumns(paramKeys.toArray(new String[paramKeys.size()]));
        Number key = jdbcInsert.execute(new MapSqlParameterSource(
                getParameterMap(t,false)));
        log.info("finished create "+t.getClass().getName());

        return getById(key.intValue());
    }


    public <T extends GenericEntity> T getById(int id) {
        return (T) this.jdbcTemplate.queryForObject( "select * from "+tableName()+" where id =?",new Object[] { id }, rowMapper());
    }
    public abstract RowMapper rowMapper();
    public abstract String tableName();
}

package com.crypto.service;

import com.crypto.repository.AccountRepository;
import com.crypto.domain.Account;
import com.crypto.domain.dto.AccountEntity;
import com.crypto.exception.CoinGeneralException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

@AllArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    public Account createAccount(Account account){
        AccountEntity accountEntity = new AccountEntity(account.getName(),account.getWechatId(),account.getAddress(),LocalDateTime.now(),null);
        AccountEntity result = accountRepository.create(accountEntity);
        if(result.getLastActivityTime()!=null){
            return Account.buildFromEntity(result);
        }else{
            throw new CoinGeneralException(CoinGeneralException.RequestStage.INTERNAL,"failed to create account"+ account.toString());
        }
    }

    public Account getAccountById(int id){
        return Account.buildFromEntity((AccountEntity) accountRepository.getById(id));
    }
}

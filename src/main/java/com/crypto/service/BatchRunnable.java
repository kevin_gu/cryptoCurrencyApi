package com.crypto.service;

import com.crypto.domain.BatchRunnableStatus;
import com.crypto.domain.Coin;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class BatchRunnable implements Runnable {

    private final CoinService coinService;
    private final AtomicBoolean on = new AtomicBoolean(true);
    private final AtomicBoolean working = new AtomicBoolean();

    private final AtomicReference<LocalDateTime> lastJobTime =
            new AtomicReference<>(LocalDateTime.now());

    public BatchRunnable(CoinService coinService) {
        this.coinService = coinService;
        List<Coin> coins = this.coinService.getAllCoin();
        if (!coins.isEmpty()) {
            lastJobTime.set(coins.get(0).getCreateTime());
        }

    }

    public BatchRunnableStatus getStatus(){

        return BatchRunnableStatus.builder().lastRun(lastJobTime.get()).on(on.get()).working(working.get()).build();
    }

    @Override
    public void run() {
        log.info("is on? " + isOn());
        if (isOn()) {

            log.info("runJobs() ");

            runJobs();
        }
        log.info("running finished");

    }

    public void runJobs() {
        try {
            working.set(true);
            Boolean isUpdated = coinService.pullFromCoinMarketCap();
            List<Coin> coins = this.coinService.getAllCoin();
            if (isUpdated&&!coins.isEmpty()) {
                lastJobTime.set(coins.get(0).getCreateTime());
            }

            working.set(false);

        } catch (Exception e) {
            log.error("batch run failed "+e);
            turnOff();
            working.set(false);

        }
    }

    public void turnOn() {
        on.set(true);
    }

    public void turnOff() {
        on.set(false);
    }

    public boolean isOn() {
        return on.get();
    }

}

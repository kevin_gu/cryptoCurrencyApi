package com.crypto.configuration;

import com.crypto.service.BatchRunnable;
import com.crypto.service.CoinService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Configuration
@EnableScheduling
@Slf4j
@Import({ServiceConfig.class})
public class ScheduleConfig implements SchedulingConfigurer {

    @Autowired
    private CoinService coinService;

    @Bean
    public BatchRunnable batchRunnable() {
        return new BatchRunnable(coinService);
    }

    @Bean
    public TaskScheduler dataSyncScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setThreadNamePrefix("allScheduler");
        scheduler.setPoolSize(1);
        return scheduler;
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.setTaskScheduler(dataSyncScheduler());
        scheduledTaskRegistrar.addTriggerTask(batchRunnable(), triggerContext -> {
            Calendar nextExecutionTime = new GregorianCalendar();
            Date lastCompletionTime = triggerContext.lastCompletionTime();
            nextExecutionTime.setTime(lastCompletionTime != null ? lastCompletionTime : new Date());
            nextExecutionTime.add(Calendar.HOUR, 1); //wait for 1 min after last job's completion
            return nextExecutionTime.getTime();
        });
    }
}

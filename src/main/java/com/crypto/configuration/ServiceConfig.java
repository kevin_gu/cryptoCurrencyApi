package com.crypto.configuration;

import com.crypto.repository.AccountRepository;
import com.crypto.repository.CoinDesriptionRepository;
import com.crypto.repository.CoinRepository;
import com.crypto.service.AccountService;
import com.crypto.service.CoinService;
import com.crypto.service.client.CoinMarketCapClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.client.RestTemplate;

@Configuration
@Import({RepositoryConfig.class})
public class ServiceConfig {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CoinDesriptionRepository coinDesriptionRepository;

    @Autowired
    private CoinRepository coinRepository;

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    public CoinMarketCapClient coinMarketCapClient(){
        return new CoinMarketCapClient();
    }

    @Bean
    public AccountService accountService(){
        return new AccountService(accountRepository);
    }

    @Bean
    public CoinService coinService(){
        return new CoinService(coinRepository,coinDesriptionRepository, coinMarketCapClient(),resourceLoader);
    }

}
